// ==UserScript==
// @name           Selfie: streamline submitting lossy approval reports
// @description    Add LWA or LMA link to quickly submit Lossy WEB/Master Approval report with spectrograms from release description.
// @version        1.98
// @match          https://redacted.sh/torrents.php?id=*
// @match          https://redacted.sh/user.php?action=edit&*
// @match          https://orpheus.network/torrents.php?id=*
// @match          https://orpheus.network/user.php?action=edit&*
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  "use strict";

  const LMAConfig = JSON.parse(localStorage.getItem("saved_lma_config") || '{"promptnotes": true, "fulldesc": false, "defaultText": ""}');
  const authKey = document.getElementById("nav_logout").firstElementChild.href.match(/auth=([a-z0-9\-_]+)/i)[1];
  const myName = document.querySelector("#nav_userinfo > a.username").text;
  const myUID = document.querySelector("#nav_userinfo > a.username").href.split("=")[1];

  if (location.href.includes(`action=edit&userid=${myUID}`)) {
    const updateLMAConfig = (e) => {
      const config = {};
      config.promptnotes = document.getElementById("promptnotes").checked;
      config.fulldesc = document.getElementById("fulldesc").checked;
      config.defaultText = document.getElementById("lossyprefix").value;
      localStorage.setItem("saved_lma_config", JSON.stringify(config));
    };
    // ref: pootie's External Stylesheet Switcher
    const searchRow = $("#tor_searchtype_tr");
    const LossyPrefixRow = $(`
      <tr>
        <td class="label tooltip">
          <strong>Lossy Approval Default Text:</strong>
        </td>
      </tr>
    `);
    const LossyPrefixCol = $(`<td></td>`);
    const LossyPrefixTxt = $(`<input type="text" size="40" name="lossyprefix" id="lossyprefix" value="">`)
    const LossyRow = $(`
      <tr>
        <td class="label tooltip">
          <strong>Lossy Approval Report Preferences:</strong>
        </td>
      </tr>
    `);
    const LossyCol = $(`<td></td>`);
    const lossyUl = $(`<ul class="options_list nobullet"></ul>`);
    const LossyNotes = $(`
      <li>
        <input type="checkbox" name="promptnotes" id="promptnotes">
        <label for="promptnotes">Prompt for notes before submitting Lossy report</label>
      </li>
    `);
    const LossyDesc = $(`
      <li>
        <input type="checkbox" name="fulldesc" id="fulldesc">
        <label for="fulldesc">Use existing torrent description for main lossy report text</label>
      </li>
    `);
    lossyUl.append([LossyNotes, LossyDesc]);
    LossyCol.append(lossyUl);
    LossyRow.append(LossyCol);
    LossyPrefixCol.append(LossyPrefixTxt);
    LossyPrefixRow.append(LossyPrefixCol);
    searchRow.after(LossyPrefixRow)
    searchRow.after(LossyRow);
    document.getElementById("promptnotes").checked = LMAConfig.promptnotes;
    document.getElementById("fulldesc").checked = LMAConfig.fulldesc;
    document.getElementById("lossyprefix").value = LMAConfig.defaultText || "";
    document.getElementById("userform").addEventListener("submit", updateLMAConfig);
  } else if (location.href.includes("torrents.php?id=")) {
    const queryAPI = async (torrentid) => {
      try {
        const res = await fetch(`/ajax.php?action=torrent&id=${torrentid}`);
        return res.json();
      } catch (error) {
        console.log(error);
      }
    };

    // cf. https://developer.mozilla.org/en-US/docs/Web/API/btoa
    // convert a Unicode string to a string in which
    // each 16-bit unit occupies only one byte
    function toBinary(string) {
      const codeUnits = Uint16Array.from(
        { length: string.length },
        (element, index) => string.charCodeAt(index)
      );
      const charCodes = new Uint8Array(codeUnits.buffer);

      let result = "";
      charCodes.forEach((char) => {
        result += String.fromCharCode(char);
      });
      return result;
    }

    function fromBinary(binary) {
      const bytes = Uint8Array.from({ length: binary.length }, (element, index) =>
        binary.charCodeAt(index)
      );
      const charCodes = new Uint16Array(bytes.buffer);
    
      let result = "";
      charCodes.forEach((char) => {
        result += String.fromCharCode(char);
      });
      return result;
    }

    const decodeJSON = (payload) => {
      var txt = document.createElement("textarea");
      txt.innerHTML = payload;
      return txt.value;
    };

    const submitLossyReport = async (torrentid, reportType, extra) => {
      const data = new FormData();
      data.append("submit", "true");
      data.append("auth", authKey);
      data.append("torrentid", torrentid);
      data.append("categoryid", 1);
      data.append("type", reportType);
      data.append("proofimages", "");
      // I am told OPS uses a different key.
      data.append("image", "");
      data.append("extra", extra);
      const response = await fetch(`/reportsv2.php?action=takereport`, {
        method: "POST",
        body: data,
      });
      return response;
    };

    const reportSiblings = async (target, parentID, reportType) => {
      const edition = target.closest("tr").classList[3];
      const torrentRows = document.querySelectorAll(`.torrent_row.${edition}`);
      torrentRows.forEach((torrentRow) => {
        let id = torrentRow.id.replace("torrent", "");
        // ignore the parent FLAC
        if (parentID == id) return;
        let alreadyApproved = torrentRow.querySelector(".tl_approved");
        let alreadyReported = torrentRow.nextElementSibling
          .querySelector(".reportinfo_table")
          ?.textContent.includes("Lossy");
        let uploader = torrentRow.nextElementSibling.querySelector(
          "td > div > blockquote > a"
        ).text;
        if (alreadyApproved || alreadyReported || uploader != myName) return;
        submitLossyReport(
          id,
          reportType,
          `Same edition as [url=${location.origin}/torrents.php?torrentid=${parentID}]FLAC[/url] previously submitted for lossy master approval.`
        );
      });
    };

    const prepareLossyReport = async (event) => {
      event.target.removeEventListener("click", prepareLossyReport);
      const origLinkText = event.target.textContent
      let reportType =
        origLinkText == "LWA"
          ? "lossywebapproval"
          : "lossyapproval";
      // OPS only has LMA, regardless of media type
      if (location.hostname.includes("orpheus.network")) {
        reportType = "lossyapproval";
      }
      event.target.textContent = "--";
      const torrentid = event.target.id.split("_")[1];
      let extra = fromBinary(atob(event.target.dataset.specs));
      if (LMAConfig.promptnotes) {
        const notes = prompt("Enter notes to include with captured spectrograms.", LMAConfig.defaultText);
        if (notes == null) {
          event.target.textContent = origLinkText
          event.target.addEventListener("click", prepareLossyReport);
          return;
        } else if (notes != "") {
          extra = `${notes}\n\n${extra}`;
        }
      } else if (LMAConfig.defaultText) {
        extra = `${LMAConfig.defaultText}\n\n${extra}`;
      }
      event.target.removeAttribute("href");
      event.target.style.pointerEvents = "none";
      event.target.style.cursor = "default";
      submitLossyReport(torrentid, reportType, extra)
        .then((response) => {
          if (!response.ok) {
            const errorText = `Failed to submit lossy master report: ${response.statusText}`;
            event.target.textContent = "✗";
            throw Error(errorText);
          }
          return response;
        })
        .then((response) => {
          event.target.textContent = "✓";
          reportSiblings(event.target, torrentid, reportType);
        })
        .catch((error) => alert(error));
    };

    document.querySelectorAll(".torrentdetails").forEach((torr) => {
      let uploader = torr.querySelector("td > div > blockquote > a").text;
      let torrentid = torr.id.split("_")[1];
      let linkSpan = torr.previousElementSibling.querySelector("span");
      const alreadyApproved = torr.previousElementSibling.querySelector(".tl_approved");
      const alreadyReported = torr
        .querySelector(".reportinfo_table")
        ?.textContent.includes("Lossy");
      if (myName == uploader && alreadyApproved == null && !alreadyReported) {
        queryAPI(torrentid).then(({ response, status }) => {
          if (status !== "success") {
            console.log("API request failed; aborting.");
            return;
          }
          const { torrent, group } = response;
          const releaseDesc = decodeJSON(torrent.description);
          const regex = /\[hide=Spec[\s\S]*hide]/i;
          let spectrograms = "";
          const found = releaseDesc.match(regex);
          if (found != null && found.length == 1) {
            let linkText = "LMA";
            if (torrent.media == "WEB") {
              linkText = "LWA";
            }
            spectrograms = found[0];
            if (LMAConfig.fulldesc) spectrograms = releaseDesc;
            linkSpan.insertAdjacentHTML(
              "afterbegin",
              `<a id="lwa_${torrent.id}" data-specs="${btoa(toBinary(spectrograms))}" class="tooltip button_pl" href="javascript:;">${linkText}</a> | `
            );
            document.getElementById(`lwa_${torrent.id}`).addEventListener("click", prepareLossyReport);
          }
        });
      }
    });
  }
})();
